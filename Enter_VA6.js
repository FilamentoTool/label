//Enter submit
const Submit_VA6_Enter= async () => {
    const temp = document.getElementById('Enter_PartNumber').value;
    const arr = temp.trim().split('-')
    lu = await lumen;
    In = await Info;
    ra = await raw;
    if (arr.length===10){

        const fa= arr[0]
        const mo= arr[1]
        const po= arr[2]
        const cri=arr[3]
        const cct=arr[4]
        const dri=arr[5]

        //VA6 Label Layout Filter
        const VA6_arr = ['E26','E27', 'E39', 'E40']
        document.getElementById(`VA6_${mo}`).style.display='initial'
        VA6_arr.filter(ele=>ele!==mo).forEach(ele=>document.getElementById(`VA6_${ele}`).style.display='none' )



        document.getElementById('Part').innerHTML=`${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000`
       
         //Part Number E27 Box Label Output & Chinese
         document.getElementById('PartNumber_box_E27_Input').innerHTML=`<input class="input_text" id="VA6_E27_PartNumber_box_Input_1" type="text" size=35 style="height: 15px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000"></input>`;
         document.getElementById('PartNumber_box_E27_C').innerHTML=`<input class="input_text" id="PartNumber_box_E27_C_1" type="text" size=35 style="height: 15px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000"></input>`;        

         
         //Part Number E40 Box Label Output & Chinese
         document.getElementById('PartNumber_box_E40_Input').innerHTML=`<input class="input_text" id="VA6_E40_PartNumber_box_Input_1" type="text" size=35 style="height: 15px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000"></input>`;
         document.getElementById('PartNumber_box_E40_C').innerHTML=`<input class="input_text" id="PartNumber_box_E40_C_1" type="text" size=35 style="height: 15px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000"></input>`;        

        //Title & Product code
        const code = ra.filter((item)=>{
            const temp=document.getElementById('Part').innerHTML
            console.log(temp)
            return item&&item.PartNumber===temp
        })


        document.getElementById('Title').innerHTML=code[0]?`${code[0].Title.toUpperCase()}`:`<span class="error">N/A</span>`;
        //Title E27 LP4 Input
        document.getElementById('VA6_E27_Title_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E27_Title_Input_1" size=30 style="height: 10px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modell, Maquette, <span>型號，型号</span>)</strong>`:`<span class="error">N/A</span>`
        
        //Title E26 LP4 Input
        document.getElementById('VA6_E26_Title_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E26_Title_Input_1" size=30 style="height: 10px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modelo, <span>모델, モデル</span>)</strong>`:`<span class="error">N/A</span>`
       
        //Title E39 LP1
       document.getElementById('VA6_E39_Title_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E39_Title_Input_1" size=30 style="height: 10px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modelo, <span>모델, モデル</span>)</strong>`:`<span class="error">N/A</span>`
      
         //Title E40 LP1
       document.getElementById('VA6_E40_Title_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E40_Title_Input_1" size=30 style="height: 10px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modelo, <span>모델, モデル</span>)</strong>`:`<span class="error">N/A</span>`
       
        
        //Title E26 Box Input
        document.getElementById('VA6_E26_BoxTitle_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E26_BoxTitle_Input_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        document.getElementById('VA6_E39_BoxTitle_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E39_BoxTitle_Input_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        
        
        
        //Title E27 Box Input & Chinese
        document.getElementById('box_title_E27_Input').innerHTML=code[0]?`<input class="input_text" id="box_title_E27_Input_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        document.getElementById('box_title_E27_C').innerHTML=code[0]?`<input class="input_text" id="box_title_E27_C_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;

        //Title E40 Box Input & Chinese
        document.getElementById('box_title_E40_Input').innerHTML=code[0]?`<input class="input_text" id="box_title_E40_Input_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        document.getElementById('box_title_E40_C').innerHTML=code[0]?`<input class="input_text" id="box_title_E40_C_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;

        const zeroPad = (num, places) => String(num).padStart(places, '0')

        document.getElementById('Code').innerHTML=code[0]?`${zeroPad(code[0].Code, 5)}`:`<span class="error">N/A</span>`

        //Part Number E27 LP4 Input
        document.getElementById('VA6_E27_PartNumber_Input').innerHTML=`<input class="input_text" id="VA6_E27_PartNumber_Input_1" type="text" size=50 style="height: 10px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000 (${zeroPad(code[0].Code, 5)})"></input>`;
        //Part Number E26 LP4 Input
        document.getElementById('VA6_E26_PartNumber_Input').innerHTML=`<input class="input_text" id="VA6_E26_PartNumber_Input_1" type="text" size=50 style="height: 10px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000 (${zeroPad(code[0].Code, 5)})"></input>`;
        
        //Part Number E39 LP1 Input
        document.getElementById('VA6_E39_PartNumber_Input').innerHTML=`<input class="input_text" id="VA6_E39_PartNumber_Input_1" type="text" size=50 style="height: 10px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000 (${zeroPad(code[0].Code, 5)})"></input>`;
        
        //Part Number E40 LP4 Input
        document.getElementById('VA6_E40_PartNumber_Input').innerHTML=`<input class="input_text" id="VA6_E40_PartNumber_Input_1" type="text" size=50 style="height: 10px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000 (${zeroPad(code[0].Code, 5)})"></input>`;
        
        
        
        //Part Number E26 Box Label
        document.getElementById('VA6_E26_BoxPartNumber_Input').innerHTML=`<input class="input_text" id="VA6_E26_BoxPartNumber_Input_1" type="text" size=50 style="height: 15px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000"></input>`;
        
        //Part Number E39 Box Label
        document.getElementById('VA6_E39_BoxPartNumber_Input').innerHTML=`<input class="input_text" id="VA6_E39_BoxPartNumber_Input_1" type="text" size=50 style="height: 15px; padding:0" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000"></input>`;
        


       //Product code E27 Box Output & Chinese
       document.getElementById('Code_box_E27_Input').innerHTML=code[0]?`<input class="input_text" id="Code_box_E27_Input_1" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
       document.getElementById('Code_box_E27_C').innerHTML=code[0]?`<input class="input_text" id="Code_box_E27_C_1" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`  

       //Product code E40 Box Output & Chinese
        document.getElementById('Code_box_E40_Input').innerHTML=code[0]?`<input class="input_text" id="Code_box_E40_Input_1" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('Code_box_E40_C').innerHTML=code[0]?`<input class="input_text" id="Code_box_E40_C_1" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`  
   

       
       //Product code E26 Box Input
       document.getElementById('VA6_E26_BoxCode_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E26_BoxCode_Input_1" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
       
       //Product code E39 Box Input
       document.getElementById('VA6_E39_BoxCode_Input').innerHTML=code[0]?`<input class="input_text" id="VA6_E39_BoxCode_Input_1" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
       

        const e_value = In.filter((item)=>{
            return item&&item.Family===fa&&item.Mounting===mo&&item.Power===parseInt(po)&&item.Driver===dri

        })

        document.getElementById('Electrical').innerHTML= e_value[0]?`${e_value[0].Electrical_Input}`:`<span class="error">N/A</span>`

        //Electrical E27 LP4 Input
        document.getElementById('VA6_E27_Electrical_Input').innerHTML= e_value[0]?`<input class="input_text" id="VA6_E27_Electrical_Input_1" size=40 type="text" size=40 style="height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        //Electrical E26 LP4
        document.getElementById('VA6_E26_Electrical_Input').innerHTML= e_value[0]?`<input class="input_text" id="VA6_E26_Electrical_Input_1" size=40 type="text" size=40 style="height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        
         //Electrical E39 LP1
         document.getElementById('VA6_E39_Electrical_Input').innerHTML= e_value[0]?`<input class="input_text" id="VA6_E39_Electrical_Input_1" size=40 type="text" size=40 style="height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
         //Electrical E40 LP4
         document.getElementById('VA6_E40_Electrical_Input').innerHTML= e_value[0]?`<input class="input_text" id="VA6_E40_Electrical_Input_1" size=40 type="text" size=40 style="height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        


        //Electrical E26 Box Input
        document.getElementById('VA6_E26_BoxElectricity_Input').innerHTML= e_value[0]?`<input class="input_text" id="VA6_E26_BoxElectricity_Input_1" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        
        //Electrical E39 Box Input
        document.getElementById('VA6_E39_BoxElectricity_Input').innerHTML= e_value[0]?`<input class="input_text" id="VA6_E39_BoxElectricity_Input_1" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        
        
        
        //Electrical E27 Box output & Chinese
        document.getElementById('Electrical_box_E27_Input').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_E27_Input_1" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_box_E27_C').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_E27_C_1" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`

        //Electrical E27 Box output & Chinese
        document.getElementById('Electrical_box_E40_Input').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_E40_Input_1" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_box_E40_C').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_E40_C_1" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`

       //Optical Output
        const lu_value = lu.filter((item)=>{
            return item.Family===fa&&item.Power===parseInt(po)&&item.CCT===parseInt(cct)&&item.CRI.toString()===cri.toString()
        })
        document.getElementById('Optical').innerHTML=lu_value[0]? `${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
       
        //Optical Output E27 LP4 Input
        document.getElementById('VA6_E27_Optical_VA6_Input').innerHTML=lu_value[0]? `<input class="input_text" id="VA6_E27_Optical_VA6_Input_1" type="text" size=40 style="height: 10px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
         //Optical Output E27 LP4 Input
         document.getElementById('VA6_E26_Optical_Input').innerHTML=lu_value[0]? `<input class="input_text" id="VA6_E26_Optical_Input_1" type="text" size=40 style="height: 10px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
       
         //Optical Output E39 LP1 Input
         document.getElementById('VA6_E39_Optical_Input').innerHTML=lu_value[0]? `<input class="input_text" id="VA6_E39_Optical_Input_1" type="text" size=40 style="height: 10px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        
         //Optical Output E40 LP4 Input
         document.getElementById('VA6_E40_Optical_Input').innerHTML=lu_value[0]? `<input class="input_text" id="VA6_E40_Optical_Input_1" type="text" size=40 style="height: 10px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
       


        //Optical Output E26 Box Input
        document.getElementById('VA6_E26_BoxOptical_Input').innerHTML=lu_value[0]? `<input class="input_text" id="VA6_E26_BoxOptical_Input_1" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>` 
   
       
        //Optical Output E27 Box Input
       document.getElementById('Optical_box_E27_Input').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_E27_Input_1" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>` 
       document.getElementById('Optical_box_E27_C').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_E27_C_1" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>` 
      
       //Optical Output E39 Box Input
       document.getElementById('VA6_E39_BoxOptical_Input').innerHTML=lu_value[0]? `<input class="input_text" id="VA6_E39_BoxOptical_Input_1" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>` 
        
       //Optical Output E40 Box Input
       document.getElementById('Optical_box_E40_Input').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_E40_Input_1" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>` 
       document.getElementById('Optical_box_E40_C').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_E40_C_1" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>` 
      

       //Mounting
       document.getElementById('Mounting_box_E27_Input').innerHTML=`<input class="input_text" id="Mounting_box_E27_Input_1" type="text" size=10 style="height: 12px; padding:0" value=${mo}></input>`
       
       
       
       //Configuration Chinese box label Input
       document.getElementById('Config_E27_C').innerHTML=e_value[0]? `<input class="input_text" id="Config_E27_C_1" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Configuration_C}">`:`<span class="error">N/A</span>`
       document.getElementById('Config_E40_C').innerHTML=e_value[0]? `<input class="input_text" id="Config_E40_C_1" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Configuration_C}">`:`<span class="error">N/A</span>`

       //Size box label input
       document.getElementById('Size_E27_C').innerHTML=e_value[0]? `<input class="input_text" id="Size_E27_C_1" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Size_C}">`:`<span class="error">N/A</span>`
       document.getElementById('Size_E40_C').innerHTML=e_value[0]? `<input class="input_text" id="Size_E40_C_1" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Size_C}">`:`<span class="error">N/A</span>`
       
       
       
        //Warning 
        document.getElementById('Warning').innerHTML=e_value[0]?`${e_value[0].Warning}`+` ${e_value[0].Warning2}`:`<span class="error">N/A</span>`
       
        //Warning E27 LP6 Input
        document.getElementById('VA6_E27_LP6_Input').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`

        //Warning E26 LP6 Input
        document.getElementById('VA6_E26_LP3_Input').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`
      
        //Warning E39 LP2 Input
        document.getElementById('VA6_E39_LP2_Input').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`
       
        //Warning E40 LP5 Input
        document.getElementById('VA6_E40_LP5_Input').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`
       
       
        //Certification
        document.getElementById('Certification').src=e_value[0]?`${e_value[0].Certification2}`:`<span class="error">N/A</span>`
        document.getElementById('VA6_Cert_Input').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2}></img>`:``
        document.getElementById('VA6_E26_Cert_Input').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2}></img>`:``

        //E39 Certification
        document.getElementById('VA6_E39_LP3_Input_Cert').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2} width="220" ></img>`:``

        //E40 Certification
        document.getElementById('VA6_E40_LP6_Input_Cert').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2} width="160px"></img>`:``


        //E27 Box image
        document.getElementById('sec_1_E27').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`
        document.getElementById('sec_c_E27').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`
       

        //Bar Code
        if (mo==='E26'){
            document.getElementById('footer_E26_Input').innerHTML=`<svg id='barcode_E26'
            jsbarcode-format="upc"
            jsbarcode-value=${code[0].Barcode}
            jsbarcode-textmargin="0"
            jsbarcode-fontoptions="bold">
            </svg>`
            JsBarcode("#barcode_E26").init();
        }
        if (mo==='E27'){
            document.getElementById('footer_E27_Input').innerHTML=`<svg id='barcode_E27'
            jsbarcode-format="upc"
            jsbarcode-value=${code[0].Barcode}
            jsbarcode-textmargin="0"
            jsbarcode-fontoptions="bold">
            </svg>`
            JsBarcode("#barcode_E27").init();
    
            document.getElementById('footer_E27_C').innerHTML = true?`${document.getElementById('footer_E27_Input').innerHTML}
            <div class="footer_C">
            <img src=${e_value[0].Box_label_C_Cert} style="width:50px; height: 30px; margin-top: 25px;"></img>
            &nbsp;&nbsp;
            <div>
            委製/進口商:英群企业股份有限公司, 地址:新北市汐止区新台<br>
            五路1段98号20樓, 电话:02-26961888<br>
            LED燈泡之重量明顯大於所替換之光源時，應留意所增加之重<br>
            量可能降低某些燈具及燈座之機械穩定性，並可能損及LED燈<br>
            泡與燈座之接觸性及固著性。
            </div> 
            </div>`:`<span class="error">N/A</span>`

        }

        if (mo==='E39'){
            document.getElementById('footer_E39_Input').innerHTML=`<svg id='barcode_E39'
            jsbarcode-format="upc"
            jsbarcode-value=${code[0].Barcode}
            jsbarcode-textmargin="0"
            jsbarcode-fontoptions="bold">
            </svg>`
            JsBarcode("#barcode_E39").init();
        }
       
        if (mo==='E40'){
            document.getElementById('footer_E40_Input').innerHTML=`<svg id='barcode_E40'
            jsbarcode-format="upc"
            jsbarcode-value=${code[0].Barcode}
            jsbarcode-textmargin="0"
            jsbarcode-fontoptions="bold">
            </svg>`
            JsBarcode("#barcode_E40").init();
    
            document.getElementById('footer_E40_C').innerHTML = true?`${document.getElementById('footer_E40_Input').innerHTML}
            <div class="footer_C">
            <img src=${e_value[0].Box_label_C_Cert} style="width:50px; height: 30px; margin-top: 25px;"></img>
            &nbsp;&nbsp;
            <div>
            委製/進口商:英群企业股份有限公司, 地址:新北市汐止区新台<br>
            五路1段98号20樓, 电话:02-26961888<br>
            LED燈泡之重量明顯大於所替換之光源時，應留意所增加之重<br>
            量可能降低某些燈具及燈座之機械穩定性，並可能損及LED燈<br>
            泡與燈座之接觸性及固著性。
            </div> 
            </div>`:`<span class="error">N/A</span>`

        }
               

        window.onerror = function(e){
        document.getElementById("error").innerHTML = 'No Value, please select again'
        }


    }else{
        document.getElementById('error').innerHTML='PartNumber is not valid!'
        setTimeout(function(){ document.getElementById('error').innerHTML="" }, 5000);
    }
}