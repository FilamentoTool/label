//Download PDF
        function generatePDF() {
				// Choose the element that our invoice is rendered in.
				const element = document.getElementById('cert_img1');
                const PartNumber= document.getElementById('PartNumber_input').value
				// Choose the element and save the PDF for our user.
                const today= new Date();
                const zeroPad = (num, places) => String(num).padStart(places, '0')
                const year = today.getFullYear().toString().slice(-2)
                const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

                var opt = {
                margin:       0.5,
                filename:     `LP1-${PartNumber}-${date}.pdf`,
                image:        { type: 'jpeg', quality: 1 },
                html2canvas:  { scale: 10},
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
                };
				html2pdf().set(opt).from(element).save();
			}

        function generatePDF_box(){
            const element = document.getElementById('box_label_all');
                const PartNumber= document.getElementById('PartNumber_Box_input').value
				// Choose the element and save the PDF for our user.
                const today= new Date();
                const zeroPad = (num, places) => String(num).padStart(places, '0')
                const year = today.getFullYear().toString().slice(-2)
                const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

                var opt = {
                margin:       0.5,
                filename:     `LB1-${PartNumber}-${date}.pdf`,
                image:        { type: 'jpeg', quality: 1 },
                html2canvas:  { scale: 10},
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
                };
				html2pdf().set(opt).from(element).save();
        }    
       
       //Fixture Box Label
        function generatePDF_box_fix(){
            const element = document.getElementById('box_label_all_fix');
                const PartNumber= document.getElementById('PartNumber_Box_fix_input').value
				// Choose the element and save the PDF for our user.
                const today= new Date();
                const zeroPad = (num, places) => String(num).padStart(places, '0')
                const year = today.getFullYear().toString().slice(-2)
                const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

                var opt = {
                margin:       0.5,
                filename:     `LB1-${PartNumber}-${date}.pdf`,
                image:        { type: 'jpeg', quality: 1 },
                html2canvas:  { scale: 10},
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
                };
				html2pdf().set(opt).from(element).save();
        }
        
        //E40 Product Label
        function generatePDF_E40_FEU() {
            // Choose the element that our invoice is rendered in.
            const element = document.getElementById('cert_img1_E40');
            const PartNumber= document.getElementById('PartNumber_input_E40').value
            // Choose the element and save the PDF for our user.
            const today= new Date();
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            const year = today.getFullYear().toString().slice(-2)
            const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

            var opt = {
            margin:       0.5,
            filename:     `LP1-${PartNumber}-${date}.pdf`,
            image:        { type: 'jpeg', quality: 1 },
            html2canvas:  { scale: 10},
            jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
            };
            html2pdf().set(opt).from(element).save();
        }

        //E40 Box Label
        function generatePDF_box_E40() {
            // Choose the element that our invoice is rendered in.
            const element = document.getElementById('box_label_all_E40');
            const PartNumber= document.getElementById('PartNumber_box_E40_1').value
            // Choose the element and save the PDF for our user.
            const today= new Date();
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            const year = today.getFullYear().toString().slice(-2)
            const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

            var opt = {
            margin:       0.5,
            filename:     `LB1-${PartNumber}-${date}.pdf`,
            image:        { type: 'jpeg', quality: 1 },
            html2canvas:  { scale: 10},
            jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
            };
            html2pdf().set(opt).from(element).save();
        }

        //FEU Box Label
        function generatePDF_box_FEU(){
            const element = document.getElementById('box_label_all_FEU');
            const PartNumber= document.getElementById('PartNumber_box_feu_C_input').value
            // Choose the element and save the PDF for our user.
            const today= new Date();
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            const year = today.getFullYear().toString().slice(-2)
            const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

            var opt = {
            margin:       0.5,
            filename:     `LB1-${PartNumber}-${date}.pdf`,
            image:        { type: 'jpeg', quality: 1 },
            html2canvas:  { scale: 10},
            jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
            };
            html2pdf().set(opt).from(element).save();
        }


          //VA6 E27 Box Label
          function generatePDF_box_E27(){
            const element = document.getElementById('VA6_E27_BoxLabel');
            const PartNumber= document.getElementById('VA6_E27_PartNumber_box_Input_1').value
            // Choose the element and save the PDF for our user.
            const today= new Date();
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            const year = today.getFullYear().toString().slice(-2)
            const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

            var opt = {
            margin:       0.5,
            filename:     `LB1-${PartNumber}-${date}.pdf`,
            image:        { type: 'jpeg', quality: 1 },
            html2canvas:  { scale: 10},
            jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
            };
            html2pdf().set(opt).from(element).save();
        }

        
        

        //VA6 E27 Product Label
        function generatePDF_LP_E27(){
            const PartNumber= document.getElementById('VA6_E27_PartNumber_Input_1').value
            // Choose the element and save the PDF for our user.
            const today= new Date();
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            const year = today.getFullYear().toString().slice(-2)
            const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);
         
            for (let i=4; i<7; i++){
                const ele = document.getElementById(`VA6_E27_LP${i}_Output`);
                var opt = {
                    margin:       0.5,
                    filename:     `LP${i}-${PartNumber}-${date}.pdf`,
                    image:        { type: 'jpeg', quality: 1 },
                    html2canvas:  { scale: 10},
                    jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
                    };
                html2pdf().set(opt).from(ele).save();
                console.log("Hello")
            }
        }

        //VA6 E26 Product Label
        function generatePDF_LP_E26(){
            const PartNumber= document.getElementById('VA6_E26_PartNumber_Input_1').value.split('(')[0].trim();
            // Choose the element and save the PDF for our user.
            const today= new Date();
            const zeroPad = (num, places) => String(num).padStart(places, '0')
            const year = today.getFullYear().toString().slice(-2)
            const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);
         
            for (let i=1; i<4; i++){
                const ele = document.getElementById(`VA6_E26_LP${i}_Output`);
                var opt = {
                    margin:       0.5,
                    filename:     `LP${i}-${PartNumber}-${date}.pdf`,
                    image:        { type: 'jpeg', quality: 1 },
                    html2canvas:  { scale: 10},
                    jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
                    };
                html2pdf().set(opt).from(ele).save();
            }
        }

      function generatePDF_Box_E26(){
        const element = document.getElementById('VA6_E26_BoxLabel');
        const PartNumber= document.getElementById('VA6_E26_BoxPartNumber_Input_1').value
        // Choose the element and save the PDF for our user.
        const today= new Date();
        const zeroPad = (num, places) => String(num).padStart(places, '0')
        const year = today.getFullYear().toString().slice(-2)
        const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

        var opt = {
        margin:       0.5,
        filename:     `LB1-${PartNumber}-${date}.pdf`,
        image:        { type: 'jpeg', quality: 1 },
        html2canvas:  { scale: 10},
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
        };
        html2pdf().set(opt).from(element).save();
      }
 
      function generatePDF_LP_E39 (){
        const PartNumber= document.getElementById('VA6_E39_PartNumber_Input_1').value.split('(')[0].trim();
        // Choose the element and save the PDF for our user.
        const today= new Date();
        const zeroPad = (num, places) => String(num).padStart(places, '0')
        const year = today.getFullYear().toString().slice(-2)
        const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);
     
        for (let i=1; i<5; i++){
            const ele = document.getElementById(`VA6_E39_LP${i}_Output`);
            var opt = {
                margin:       0.5,
                filename:     `LP${i}-${PartNumber}-${date}.pdf`,
                image:        { type: 'jpeg', quality: 1 },
                html2canvas:  { scale: 10},
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
                };
            html2pdf().set(opt).from(ele).save();
        }
      }

      function generatePDF_LP_E40(){
        const PartNumber= document.getElementById('VA6_E40_PartNumber_Input_1').value.split('(')[0].trim();
        // Choose the element and save the PDF for our user.
        const today= new Date();
        const zeroPad = (num, places) => String(num).padStart(places, '0')
        const year = today.getFullYear().toString().slice(-2)
        const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);
     
        for (let i=4; i<8; i++){
            const ele = document.getElementById(`VA6_E40_LP${i}_Output`);
            var opt = {
                margin:       0.5,
                filename:     `LP${i+1}-${PartNumber}-${date}.pdf`,
                image:        { type: 'jpeg', quality: 1 },
                html2canvas:  { scale: 10},
                jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
                };
            html2pdf().set(opt).from(ele).save();
        }
      }

      function generatePDF_Box_E39(){
        const element = document.getElementById('VA6_E39_BoxLabel');
        const PartNumber= document.getElementById('VA6_E39_BoxPartNumber_Input_1').value
        // Choose the element and save the PDF for our user.
        const today= new Date();
        const zeroPad = (num, places) => String(num).padStart(places, '0')
        const year = today.getFullYear().toString().slice(-2)
        const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

        var opt = {
        margin:       0.5,
        filename:     `LB1-${PartNumber}-${date}.pdf`,
        image:        { type: 'jpeg', quality: 1 },
        html2canvas:  { scale: 10},
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
        };
        html2pdf().set(opt).from(element).save();
      }

      function generatePDF_box_E40(){
        const element = document.getElementById('VA6_E40_BoxLabel');
        const PartNumber= document.getElementById('VA6_E40_PartNumber_box_Input_1').value
        // Choose the element and save the PDF for our user.
        const today= new Date();
        const zeroPad = (num, places) => String(num).padStart(places, '0')
        const year = today.getFullYear().toString().slice(-2)
        const date = year+zeroPad((today.getMonth()+1),2)+zeroPad(today.getDate(),2);

        var opt = {
        margin:       0.5,
        filename:     `LB1-${PartNumber}-${date}.pdf`,
        image:        { type: 'jpeg', quality: 1 },
        html2canvas:  { scale: 10},
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
        };
        html2pdf().set(opt).from(element).save();

      }
       
        function Clear(){
            location.reload();
        }
        //Family
         doThisOnChangeFa = function( value, optionIndex )
        {
            if ( optionIndex != null )
            {
                var option = document.getElementById("Family").options[optionIndex];
                option.selected = true;
                value = option.value;
                
            }
            console.log(value)
        }

        //Mounting change
        doThisOnChangeMount= function( value, optionIndex )
        {
            if ( optionIndex != null )
            {
                var option = document.getElementById("Mounting").options[optionIndex];
                option.selected = true;
                value = option.value;
                
            }
            console.log(value)
        }

        //Power change
        doThisOnChangePower = function( value, optionIndex )
        {
            if ( optionIndex != null )
            {
                var option = document.getElementById( "Power" ).options[optionIndex];
                option.selected = true;
                value = option.value;
                
            }
            console.log(value)
        }

        //CRI change
        doThisOnChangeCRI = function( value, optionIndex )
        {
            if ( optionIndex != null )
            {
                var option = document.getElementById( "CRI" ).options[optionIndex];
                option.selected = true;
                value = option.value;
                
            }
            console.log(value)
        }

        //CCT change
        doThisOnChangeCCT = function( value, optionIndex )
        {
            if ( optionIndex != null )
            {
                var option = document.getElementById("CCT" ).options[optionIndex];
                option.selected = true;
                value = option.value;
                
            }
            console.log(value)
        }

         //Driver change
         doThisOnChangeDriver = function( value, optionIndex )
        {
            if ( optionIndex != null )
            {
                var option = document.getElementById("Driver" ).options[optionIndex];
                option.selected = true;
                value = option.value;
                
            }
            console.log(value)
        }
       