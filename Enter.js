//Enter submit
const Submit_Enter= async () => {
    const temp = document.getElementById('Enter_PartNumber').value;
    const arr = temp.trim().split('-')
    lu = await lumen;
    In = await Info;
    ra = await raw;
    if (arr.length===11){

        const fa= arr[0]
        const mo= arr[1]
        const po= arr[2]
        const cri=arr[3]
        const cct=arr[4]
        const dri=arr[5]
       
        document.getElementById('Part').innerHTML=`${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL`
        document.getElementById('PartNumber_box').innerHTML=`<input class="input_text" id="PartNumber_Box_input" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;
        document.getElementById('PartNumber_card').innerHTML=`<input class="input_text" id="PartNumber_input" type="text" size=30 style="height: 12px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;
        //E40 LP
        document.getElementById('PartNumber_card_E40').innerHTML=`<input class="input_text" id="PartNumber_input_E40" type="text" size=30 style="height: 12px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;     
        
        //E40 Box PartNumber
        document.getElementById('PartNumber_box_E40').innerHTML=`<input class="input_text" id="PartNumber_box_E40_1" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;        

        //E40 Chinese Box PartNumber
        document.getElementById('PartNumber_box_E40_C').innerHTML=`<input class="input_text" id="PartNumber_box_E40_C_input" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;        

        //Fixture partNumber
        document.getElementById('PartNumber_box_fix').innerHTML=`<input class="input_text" id="PartNumber_Box_fix_input" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;        
        
         //FEU Box PartNumber
         document.getElementById('PartNumber_box_feu').innerHTML=`<input class="input_text" id="PartNumber_box_feu_1" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;        

         //FEU Chinese Box PartNumber
         document.getElementById('PartNumber_box_FEU_C').innerHTML=`<input class="input_text" id="PartNumber_box_feu_C_input" size=40 type="text" style="height: 15px" value="${fa}-${mo}-${po}-${cri}-${cct}-${dri}-AN-B90-000-000-FL"></input>`;        



        //Filter Layout
        if(mo==='X39'||mo==='FIX'){
            document.getElementById(`X39_Fix_LP`).style.display='initial'
            document.getElementById(`E40_FEU_LP`).style.display='none'
        }
        else if(mo==='E40'||mo==='FEU'){
            document.getElementById(`E40_FEU_LP`).style.display='initial'
            document.getElementById(`X39_Fix_LP`).style.display='none'
        }
        const mo_arr = ['X39','E40', 'FIX', 'FEU']
        document.getElementById(`V2M_V2L_${mo}`).style.display='initial'
        mo_arr.filter(ele=>ele!==mo).forEach(ele=>document.getElementById(`V2M_V2L_${ele}`).style.display='none' )

        //Title & Product code
        const code = ra.filter((item)=>{
            const temp=document.getElementById('Part').innerHTML
            console.log(temp)
            return item&&item.PartNumber===temp
        })

        document.getElementById('Title').innerHTML=code[0]?`${code[0].Title.toUpperCase()}`:`<span class="error">N/A</span>`;
        document.getElementById('box_title').innerHTML=code[0]?`<input class="input_text" id="box_title_input" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;
        document.getElementById('Title_card').innerHTML=code[0]?`<input class="input_text" id="Title_card_input" style="width: 100px; height: 12px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input> <strong>(Modell, Maquette, <span>型號，型号</span>)</strong>`:`<span class="error">N/A</span>`

        //E40 LP Title
        document.getElementById('Title_card_E40').innerHTML=code[0]?`<input class="input_text" id="Title_card_E40_input" style="width: 100px; height: 12px; padding:0" type="text" value="${code[0].Title.toUpperCase()}"></input><strong>(Modell, Maquette,<span> 型號, 型号</span>)</strong>`:`<span class="error">N/A</span>`
        
        //E40 Box Title
        document.getElementById('box_title_E40').innerHTML=code[0]?`<input class="input_text" id="box_title_E40_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;

        //E40 Box Chinese Title
        document.getElementById('box_title_E40_C').innerHTML=code[0]?`<input class="input_text" id="box_title_E40_2" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;

        //Fixture Title & Product code
        document.getElementById('box_title_fix').innerHTML=code[0]?`<input class="input_text" id="box_title_fix_input" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;

        //FEU Box Title
        document.getElementById('box_title_feu').innerHTML=code[0]?`<input class="input_text" id="box_title_FEU_1" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;

        //FEU Box Chinese Title
        document.getElementById('box_title_FEU_C').innerHTML=code[0]?`<input class="input_text" id="box_title_FEU_2" type="text" value="${code[0].Title.toUpperCase()}"></input>`:`<span class="error">N/A</span>`;


        const zeroPad = (num, places) => String(num).padStart(places, '0')

        document.getElementById('Code').innerHTML=code[0]?`${zeroPad(code[0].Code, 5)}`:`<span class="error">N/A</span>`
        document.getElementById('Code_box').innerHTML=code[0]?`<input class="input_text" id="Code_box_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('PartNumber_card').innerHTML+=code[0]?`<input class="input_text" id="PartNumber_card_input" type="text" style="width: 32px; height: 12px; padding:0" value="${zeroPad(code[0].Code, 5)}"></input>`:``

        //E40 LP Product Code
        document.getElementById('PartNumber_card_E40').innerHTML+=code[0]?`<input class="input_text" id="PartNumber_card_E40_input" type="text" style="width: 32px; height: 12px; padding:0" value="${zeroPad(code[0].Code, 5)}"></input>`:``
        
        //E40 Box Product code
        document.getElementById('Code_box_E40').innerHTML=code[0]?`<input class="input_text" id="Code_box_E40_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`

        //E40 Box Chinese Product code
        document.getElementById('Code_box_E40_C').innerHTML=code[0]?`<input class="input_text" id="Code_box_E40_C_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`

         //FEU Box Product code
         document.getElementById('Code_box_feu').innerHTML=code[0]?`<input class="input_text" id="Code_box_FEU_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`

         //FEU Box Chinese Product code
         document.getElementById('Code_box_FEU_C').innerHTML=code[0]?`<input class="input_text" id="Code_box_FEU_C_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`
       
        //Fixture Product code
        document.getElementById('Code_box_fix').innerHTML=code[0]?`<input class="input_text" id="Code_box_fix_input" type="text" size=10 style="height: 15px" value="${zeroPad(code[0].Code, 5)}"></input>`:`<span class="error">N/A</span>`

        //Barcode
        console.log((code[0].Barcode))
        document.getElementById('footer').innerHTML=`<svg id='barcode'
        jsbarcode-format="upc"
        jsbarcode-value=${code[0].Barcode}
        jsbarcode-textmargin="0"
        jsbarcode-fontoptions="bold">
        </svg>`
        JsBarcode("#barcode").init();

        document.getElementById('footer_fix').innerHTML=`<svg id='barcode_fix'
        jsbarcode-format="upc"
        jsbarcode-value=${code[0].Barcode}
        jsbarcode-textmargin="0"
        jsbarcode-fontoptions="bold">
        </svg>`
        JsBarcode("#barcode_fix").init();

        //footer_E40
        document.getElementById('footer_E40').innerHTML=`<svg id='barcode_E40'
        jsbarcode-format="upc"
        jsbarcode-value=${code[0].Barcode}
        jsbarcode-textmargin="0"
        jsbarcode-fontoptions="bold">
        </svg>`
        JsBarcode("#barcode_E40").init();

        //footer_FEU
        document.getElementById('footer_feu').innerHTML=`<svg id='barcode_FEU'
        jsbarcode-format="upc"
        jsbarcode-value=${code[0].Barcode}
        jsbarcode-textmargin="0"
        jsbarcode-fontoptions="bold">
        </svg>`
        JsBarcode("#barcode_FEU").init();


        const e_value = In.filter((item)=>{
            return item&&item.Family===fa&&item.Mounting===mo&&item.Power===parseInt(po)&&item.Driver===dri

        })


        document.getElementById('Electrical').innerHTML= e_value[0]?`${e_value[0].Electrical_Input}`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_box').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_input" size=35 type="text" style="height: 15px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        document.getElementById('Electrical_card').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_card_input" type="text" style="width: 150px; height: 10px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`
        
        //Electrical Input E40
        document.getElementById('Electrical_card_E40').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_card_E40_input" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`

        //Electrical Input E40 Box
        document.getElementById('Electrical_box_E40').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_E40_input" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`

        console.log(e_value[0].Electrical_Input_C)
        //Electrical Input E40 Box Chinese
        document.getElementById('Electrical_box_E40_C').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_E40_C_input" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input_C}"></input>`:`<span class="error">N/A</span>`

        //Electrical Input FEU Box
        document.getElementById('Electrical_box_feu').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_FEU_input" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`

        console.log(e_value[0].Electrical_Input_C)
        //Electrical Input FEU Box Chinese
        document.getElementById('Electrical_box_FEU_C').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_FEU_C_input" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input_C}"></input>`:`<span class="error">N/A</span>`
        
        //Electrical Input Fix
        document.getElementById('Electrical_box_fix').innerHTML= e_value[0]?`<input class="input_text" id="Electrical_box_fix_input" size=35 type="text" style="height: 12px; padding:0" value="${e_value[0].Electrical_Input}"></input>`:`<span class="error">N/A</span>`




       //Optical Output
        const lu_value = lu.filter((item)=>{
            return item.Family===fa&&item.Power===parseInt(po)&&item.CCT===parseInt(cct)&&item.CRI.toString()===cri.toString()
        })
        document.getElementById('Optical').innerHTML=lu_value[0]? `${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        document.getElementById('Optical_box').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_input" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        document.getElementById('Optical_card').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_card_input" type="text" style="width: 160px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        
        //Optical Output E40 LP
        document.getElementById('Optical_card_E40').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_card_E40_input" type="text" style="width: 180px; height: 10px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`

        //Optical Output E40 Box
        document.getElementById('Optical_box_E40').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_E40_input" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        
        //Optical Output E40 Box Chinese
        document.getElementById('Optical_box_E40_C').innerHTML=e_value[0]? `<input class="input_text" id="Optical_box_E40_C_input" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Optical_Output_C}">`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`

        //Optical Output FEU Box
        document.getElementById('Optical_box_feu').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_feu_input" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        
        //Optical Output FEU Box Chinese
        document.getElementById('Optical_box_FEU_C').innerHTML=e_value[0]? `<input class="input_text" id="Optical_box_FEU_C_input" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Optical_Output_C}">`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
        
        //Optical Output Fix
        document.getElementById('Optical_box_fix').innerHTML=lu_value[0]? `<input class="input_text" id="Optical_box_fix_input" type="text" style="width: 180px; height: 12px; padding:0" value="${lu_value[0].Lumen} Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0"></input>`:`<span class="error">N/A Lumens, xxx cd, CCT ${cct}00K, CRI>${cri}0</span>`
       
        //Configuration E40 Box Chinese
        document.getElementById('Config_E40_C').innerHTML=e_value[0]? `<input class="input_text" id="Config_box_E40_C_input" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Configuration_C}">`:`<span class="error">N/A</span>`

        //Dimension E40 Box Chinese
        document.getElementById('Size_E40_C').innerHTML=e_value[0]? `<input class="input_text" id="Size_box_E40_C_input" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Size_C}">`:`<span class="error">N/A</span>`

        //Configuration FEU Box Chinese
        document.getElementById('Config_FEU_C').innerHTML=e_value[0]? `<input class="input_text" id="Config_box_FEU_C_input" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Configuration_C}">`:`<span class="error">N/A</span>`

        //Dimension FEU Box Chinese
        document.getElementById('Size_FEU_C').innerHTML=e_value[0]? `<input class="input_text" id="Size_box_FEU_C_input" type="text" style="width: 180px; height: 12px; padding:0" value="${e_value[0].Size_C}">`:`<span class="error">N/A</span>`
        



        //Warning 
        document.getElementById('Warning').innerHTML=e_value[0]?`${e_value[0].Warning}`+` ${e_value[0].Warning2}`:`<span class="error">N/A</span>`
        document.getElementById('Warning_card').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`

        //Warning E40
        document.getElementById('Warning_card_E40').innerHTML=e_value[0]?`${e_value[0].Warning}`+`<span class='KJ_Fonts_Warning'>${e_value[0].Warning2}</span>`:`<span class="error">N/A</span>`
        
        //Certification
        document.getElementById('Certification').src=e_value[0]?`${e_value[0].Certification2}`:`<span class="error">N/A</span>`
        document.getElementById('cert').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2}></img>`:``

        //E40 LP Certification  cert1_E40
        document.getElementById('cert_E40').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2} style="width:83px"></img>`:``
        document.getElementById('cert1_E40').innerHTML=e_value[0]?`<img src=${e_value[0].Certification2} style="width:83px"></img>`:``


        //Box Label image
        document.getElementById('sec_2').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`

        //E40 Box Label image
        document.getElementById('sec_1_E40').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`

        //E40 Box Chinese Label image
        document.getElementById('sec_c_E40').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`
  
        //FEU Box Label image
        document.getElementById('sec_2_feu').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`

         //FEU Box Chinese Label image
         document.getElementById('sec_c_FEU').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`

        //Fixture Box Label image
        document.getElementById('sec_2_fix').src=e_value[0]?`${e_value[0].Box_label_img}`:`<span class="error">N/A</span>`

        //Mounting Box
        if (mo==='X39'){
            document.getElementById('Mounting_box').innerHTML=`<input class="input_text" id="Socket_Box_input" type="text" style="width: 50px; height: 12px; padding:0" value="EX39"></input>`
        }else{
            document.getElementById('Mounting_box').innerHTML=`<input class="input_text" id="Socket_Box_input" type="text" style="width: 50px; height: 12px; padding:0" value="${mo}"></input>`
        }
       
        //E40 Mounting Box
        document.getElementById('Mounting_box_E40').innerHTML=`<input class="input_text" id="Socket_Box__E40_input" type="text" style="width: 50px; height: 12px; padding:0" value="${mo}"></input>`

        //E40 Box Chinese Footer
        document.getElementById('footer_E40_C').innerHTML=true?`${document.getElementById('footer_E40').innerHTML}
        <div class="footer_C">
        <img src=${e_value[0].Box_label_C_Cert} style="width:50px; height: 30px; margin-top: 25px;"></img>
        &nbsp;&nbsp;
        <div>
        委製/進口商:英群企业股份有限公司, 地址:新北市汐止區新台五路1段98號20樓<br>
        五路1段98號20樓, 电话:02-26961888<br>
        LED燈泡之重量明顯大於所替換之光源時，應留意所增加之重<br>
        量可能降低某些燈具及燈座之機械穩定性，並可能損及LED燈<br>
        泡與燈座之接觸性及固著性。
        </div> 
        </div>`:`<span class="error">N/A</span>`

        //FEU Box Chinese Footer
        document.getElementById('footer_FEU_C').innerHTML=true?`${document.getElementById('footer_feu').innerHTML}
        <div class="footer_C">
        <img src=${e_value[0].Box_label_C_Cert} style="width:50px; height: 30px; margin-top: 25px;"></img>
        &nbsp;&nbsp;
        <div>
        委製/進口商:英群企业股份有限公司, 地址:新北市汐止區新台<br>
        五路1段98號20樓, 电话:02-26961888<br>
        LED燈泡之重量明顯大於所替換之光源時，應留意所增加之重<br>
        量可能降低某些燈具及燈座之機械穩定性，並可能損及LED燈<br>
        泡與燈座之接觸性及固著性。
        </div> 
        </div>`:`<span class="error">N/A</span>`



        window.onerror = function(e){
        document.getElementById("error").innerHTML = 'No Value, please select again'
        }
        


        
    }else{
        document.getElementById('error').innerHTML='PartNumber is not valid!'
        setTimeout(function(){ document.getElementById('error').innerHTML="" }, 5000);
    }

   
 } 
